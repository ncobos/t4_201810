package model.data_structures;

import java.util.Iterator;

public interface Queue <T extends Comparable<T>>{
	
	/**
	 * Dice si la lista esta vacia 
	 * @return true si la lista esta vacia,false de lo contrario
	 */
	public boolean isEmpty();
	/**
	 * Retorna el tamaño que tiene la lista en ese momento 
	 * @return tamaño de actual de la lista 
	 */
	public int size();
	/**
	 * Agrega un elemento al final de la lista 
	 */
	public void enqueue(T ele);
	/**
	 * Elimina el primer elemento de la lista 
	 * @return El nuevo primer elemento de la lista, null sino hay.
	 */
	public T dequeue();

	
	

}
