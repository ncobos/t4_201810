package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1A	
			case 2:

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq1A = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq1A = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq1A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq1A = sc.next();

				//VORangoFechaHora
				RangoFechaHora rangoReq1A = new RangoFechaHora(fechaInicialReq1A, fechaFinalReq1A, horaInicialReq1A, horaFinalReq1A);
				Object[] arreglo1 = Controller.crearArreglo(rangoReq1A);
				
				for(int i = 1; i < arreglo1.length; i++)
				{
					ServicioResumen servicio1 = (ServicioResumen) arreglo1[i];
					
					if( i<20){
					System.out.println("Id " + i + ": "+  servicio1.getId());
					System.out.println("Servicios: " + servicio1.getNumOfServices());}
				}
				

				break;

			case 3: //2A

				Comparable[] arreglo2 = Controller.ordenar();
				System.out.println(arreglo2.length);
				//VORangoFechaHora
				for (int j = 0; j < arreglo2.length; j++)
				{
					ServicioResumen servicio2 = (ServicioResumen) arreglo2[j];
					
						System.out.println("Id " + j + ": "+  servicio2.getId());
						System.out.println("Servicios asociados: " + servicio2.getNumOfServices());
				}
				
				//TODO
				//Muestre la info del taxi

				break;

			case 4:
				
				//VORangoFechaHora
				Comparable[] arreglo2b = Controller.colaPrioridad();
				
				for(int i = 1; i < arreglo2b.length; i++)
				{
					

					CompaniaServicios servicio2 = (CompaniaServicios) arreglo2b[i];
					
						System.out.println("Nombre " + 1 + ": "+  servicio2.getNomCompania());
						System.out.println("Numero de servicios: " + servicio2.getNumberOfServices());
					
				}

				
				
				break;
			
			case 5: //salir
				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");

		System.out.println("\nTaller 4:\n");
		System.out.println("2. Obtener un arreglo en forma de binary heap (1.4).");
		System.out.println("3. Ordenar el arreglo (2.1).");
		System.out.println("4. Cola de prioridad orientada a mayor (2.2).");
		System.out.println("5. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
