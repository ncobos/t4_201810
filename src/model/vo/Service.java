package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	
	private String tripId;
	
	private int tripSeconds;
	
	private double tripMiles;
	
	private double tripTotal;
	
	private String tripStart;
	
	private String tripEnd;
	
	private int dropoffCommunityArea;
	
	private int pickupCommunityArea;
	
	private int dropoffCensusTract;
	
	private int dropoffCentroidLongitude;
	
	Taxi taxi = null;
	
	
	public Service (String pTripId, int pSec, double pMil, double pTot, int pComArea,int pUp, String taxiId, String pCom, String pStart, String pEnd)
	{
		tripId = pTripId;
		
		tripSeconds = pSec;
		
		tripMiles = pMil;
		
		tripTotal = pTot;
		
		dropoffCommunityArea = pComArea;
		
		pickupCommunityArea = pUp;
		
		tripStart = pStart;
		
		tripEnd = pEnd;
		
		taxi = new Taxi(taxiId, pCom);
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi.getTaxiId();
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotalCost() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return tripStart.compareToIgnoreCase(o.tripStart);
	}
	
	public int compareStart(String pStart)
	{
		int rta = tripStart.compareToIgnoreCase(pStart);
		
		return rta;
	}
	
	public int compareEnd(String pEnd)
	{
		int rta = tripEnd.compareToIgnoreCase(pEnd);
		
		return rta;
	}
	
	public int getDropComArea()
	{
		return dropoffCommunityArea;
	}
	
	public int getPickupCommunityArea() {
		return pickupCommunityArea;
	}
	
	public Taxi getTaxi()
	{
		return taxi;
	}
	
	public String getTripEnd()
	{
		return tripEnd;
	}
	
	public String getTripStart()
	{
		return tripStart;
	}
}
