package controller;

import api.ITaxiTripsManager;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.cargarSistema(direccionJson);
	}
	//1.4
	public static Object[] crearArreglo(RangoFechaHora rango)
	{
		Object[] rango1 = manager.crearArreglo(rango);
		return rango1;
	}

	//2.1
	public static Comparable[] ordenar()
	{
		Comparable[] rango2 = manager.ordenar();
		return rango2;
	}
	
	//2.2
	public static Comparable[] colaPrioridad()
	{
		Comparable[] ran = manager.empresas();
		return ran;
	}
		


}
