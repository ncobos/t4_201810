package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.BinaryHeap;
import model.data_structures.Cola;
import model.data_structures.HeapSort;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.MaxPQ;
import model.data_structures.MergeSort;
import model.data_structures.Pila;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	Cola <Service> colaJson = new Cola <Service>();

	Pila <Service> pilaJson = new Pila <Service>();

	Lista <Service> listaJson = new Lista <Service>();
	
	Lista <String> listaId = new Lista <String>();


	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		boolean rta = false;

		JsonParser parser = new JsonParser();

		if (direccionJson.equals(DIRECCION_LARGE_JSON))
		{
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-08-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-07-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-06-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-05-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-04-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-03-02-2017.json");

		}

		try {
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";

			if(direccionJson.equals(DIRECCION_LARGE_JSON))
			{
				direccionJson = "./data/taxi-trips-wrvz-psew-subset-02-02-2017.json";
			}

			System.out.println("Inside loadServices with " + direccionJson);
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(direccionJson));


			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				if(obj == null )
				{
					System.out.println("El objeto es nulo en: " + i);
				}

				/* Mostrar un JsonObject (Servicio taxi) */
				//System.out.println("------------------------------------------------------------------------------------------------");

				/* Obtener la propiedad company de un servicio (String) */
				String company = "NaN";
				if ( obj.get("company") != null)
				{ company = obj.get("company").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff_centroid_longitude de un servicio (String) */
				String dropoff_centroid_longitude = "NaN";
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff_centroid_latitude de un servicio (String)*/
				String dropoff_centroid_latitude = "NaN";
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString(); }


				/* Obtener la propiedad trip ID de un servicio (String) */
				String tripId = "NaN";
				if ( obj.get("trip_id") != null)
				{ tripId = obj.get("trip_id").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip seconds de un servicio (int) */
				int tripSec = 0;
				if ( obj.get("trip_seconds") != null)
				{ tripSec = obj.get("trip_seconds").getAsInt(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip miles de un servicio (double) */
				double tripMil= 0;
				if ( obj.get("trip_miles") != null)
				{ tripMil = obj.get("trip_miles").getAsDouble(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip total de un servicio (double) */
				double tripTot= 0;
				if ( obj.get("trip_total") != null)
				{ tripTot = obj.get("trip_total").getAsDouble(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad taxi ID de un servicio (String) */
				String taxiId = "NaN";
				if ( obj.get("taxi_id") != null)
				{ taxiId = obj.get("taxi_id").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff community area de un servicio (int) */
				int comArea = 0;
				if ( obj.get("dropoff_community_area") != null)
				{ comArea = obj.get("dropoff_community_area").getAsInt(); }

				/* Obtener la propiedad pickup_community_area de un servicio (String) */
				int pickup_community_area= 0;
				if ( obj.get("pickup_community_area") != null)
				{ pickup_community_area = obj.get("pickup_community_area").getAsInt(); }

				/* Obtener la propiedad trip_end_timestamp de un servicio (String) */
				String trip_end_timestamp= "NaN";
				if ( obj.get("trip_end_timestamp") != null)
				{ trip_end_timestamp = obj.get("trip_end_timestamp").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad  trip_start_timestamp de un servicio (String) */
				String  trip_start_timestamp= "NaN";
				if ( obj.get("trip_start_timestamp") != null)
				{  trip_start_timestamp = obj.get("trip_start_timestamp").getAsString(); }


				JsonObject dropoff_localization_obj = null; 


				/* Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio*/
				if ( obj.get("dropoff_centroid_location") != null )
				{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
				JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener cada coordenada del JsonArray como Double */
				double longitude = dropoff_localization_arr.get(0).getAsDouble();
				double latitude = dropoff_localization_arr.get(1).getAsDouble();}
				//System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


				Service servicio = new Service(tripId, tripSec, tripMil, tripTot, comArea,pickup_community_area, taxiId, company, trip_start_timestamp, trip_end_timestamp);

				listaJson.add(servicio);
			}

			System.out.println("En esta lista hay: " + listaJson.size());

			if(listaJson.size()>0)
			{
				rta = true;
			}
		}

		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		return rta;
	}

	MaxPQ heap = new MaxPQ(listaJson.size());
Object[] arregloHeap = new Object[0];
RangoFechaHora ran = null ;
	@Override //1A
	public Object[] crearArreglo(RangoFechaHora rango)
	{
		ran = rango;
		String fechaInicial = rango.getFechaInicial();
		String horaInicial = rango.getHoraInicio();

		String fechaFinal = rango.getFechaFinal();
		String horaFinal = rango.getHoraFinal();

		String inicio = fechaInicial + "T"+ horaInicial;
		String fin = fechaFinal + "T"+ horaFinal;

		Lista <Service> listaServicios = new Lista <Service>();
		for (Service serv: listaJson)
		{
			if(serv.compareStart(inicio)>=0 && serv.compareEnd(fin)<= 0)
			{
				listaServicios.add(serv);
			}
		}

		Lista <String> listaId = new Lista <String> ();
		for(Service actual : listaServicios )
		{
			String id = actual.getTaxi().getTaxiId();

			if (listaId.size()==0)
			{listaId.add(id);}
			else
			{
				Boolean encontro = false;

				for (String nom: listaId)
				{
					if(nom.equals(id))
					{
						encontro = true;
					}
				}

				if(!encontro)
				{
					listaId.add(id);
				}
			}
		}

		ServicioResumen[] servicios = new ServicioResumen[listaId.size()];
		MaxPQ heap = new MaxPQ(listaId.size());
		int cont = 0;
		int i = 0;
		//		ServicioResumen[] servicios = new ServicioResumen[listaId.size()];

		for (String codigo: listaId)
		{
			int numero = 0;
			double distancia = 0;
			double dinero = 0;
			String compania = null;
			for (Service servicioActual: listaServicios)
			{
				if(servicioActual.getTaxiId().equals(codigo))
				{
					numero++;
					distancia += servicioActual.getTripMiles();
					dinero += servicioActual.getTripTotalCost();
					compania = servicioActual.getTaxi().getCompany();
				}
			}

			ServicioResumen servicioResumen = new ServicioResumen(numero, distancia, dinero, codigo);

			servicios[i] = servicioResumen;
			i++;
			cont ++;
		}

		BinaryHeap heap2 = new BinaryHeap<>(servicios);
		arregloHeap = heap2.getPQ();
		System.out.println(arregloHeap[1]);
		System.out.println(cont);

		System.out.println();
		return arregloHeap;
	}


	public Comparable[] ordenar()
	{
		System.out.println("size: " + arregloHeap.length);
		Comparable[] arreglo = new Comparable[arregloHeap.length-1];
		for(int k =0; k < arreglo.length; k++)
		{
			arreglo[k] = (Comparable) arregloHeap[k+1];
		}
		HeapSort.sort(arreglo);
		return arreglo;
	}
	
	public Comparable[] empresas()
	{

		String fechaInicial = ran.getFechaInicial();
		String horaInicial = ran.getHoraInicio();

		String fechaFinal = ran.getFechaFinal();
		String horaFinal = ran.getHoraFinal();

		String inicio = fechaInicial + "T"+ horaInicial;
		String fin = fechaFinal + "T"+ horaFinal;

		Lista <Service> listaServicios = new Lista <Service>();
		for (Service serv: listaJson)
		{
			if(serv.compareStart(inicio)>=0 && serv.compareEnd(fin)<= 0)
			{
				listaServicios.add(serv);
			}
		}

		Lista <String> listaCom = new Lista <String> ();
		for(Service actual : listaServicios )
		{
			String com = actual.getTaxi().getCompany();

			if (listaCom.size()==0)
			{listaCom.add(com);}
			else
			{

				Boolean encontro = false;

				for (String nom: listaCom)
				{
					if(nom.equals(com))
					{
						encontro = true;
					}
				}

				if(!encontro)
				{
					listaCom.add(com);

				}
			}

		}
		CompaniaServicios[] Companias = new CompaniaServicios[listaCom.size()];
		MaxPQ heap = new MaxPQ(listaCom.size());
		int i = 0;
		for (String compa: listaCom)
		{
			 Lista<Service> servi = new Lista<Service>();
			String nom = compa;
			for (Service servicioActual: listaServicios)
			{
				if(servicioActual.getTaxi().getCompany().equals(nom))
				{
					
					servi.add(servicioActual);
				}
			}

			CompaniaServicios compania = new CompaniaServicios(nom,servi);
			Companias[i] = compania;
			
		}
		MaxPQ respuesta = new MaxPQ<CompaniaServicios>(Companias.length);
		for (CompaniaServicios actual: Companias)
		{
			respuesta.insert(actual);
		}
		
		return respuesta.getPQ();

}
}
