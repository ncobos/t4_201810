package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	
	private String company;
	
	private String taxi_Id;
	
	public Taxi(String pId, String pCom)
	{
		company = pCom;
		
		taxi_Id = pId;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_Id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return taxi_Id.compareToIgnoreCase(o.getTaxiId());
	}	
	
	public int compareCompany (Taxi o)
	{
		return company.compareToIgnoreCase(o.getCompany());
	}
}
